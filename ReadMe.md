# Phone Number Verification

this django project is a ready-to use phone number verification and token varification ,
with custom Authentication Backends Without Django REST Framework.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Prerequisites for the software

```
django = 2.2.0
SQlite = 3 (is built-in in django)
```


## Running the tests

4  unit tests are implemeneted
2  functional tests are implemented
### Break down into end to end tests

for running unit tests and functional tests simply get inside projects root directory and run:

```
python mange.py test authentication.tests
```


## Deployment

just becuase it was a mock project i didn't make a virtual enviorment.
you can run it inside root of the project by:

```
python manage.py makemigration authenticate
python manage.py migrate authenticate
python mange.py migrate
python manage.py runserver

```

in production Mode Set Debug = False in Settings.py
