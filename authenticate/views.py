import datetime
import uuid

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .customauth import CustomAuth
from .mockapi.mocksmspanel import sent_token
from .models import CustomUser, Token
from django.http import JsonResponse


@csrf_exempt
def sendcode(request):
    """
    send 4-digit code to a specific phone number
    :param request:
    :return:
    """
    phonenumber = request.POST.get("phone_number")

    # check if user wants to authenticte with phone_number
    if phonenumber:
        exist = CustomUser.objects.filter(phone_number=phonenumber).exists()
        # checks if phone number exists in data base
        if exist:
            result = sent_token(phone_number=phonenumber)
            return JsonResponse({
                "status": "ok",
                "description": result
            })
        else:
            new_user = CustomUser(phone_number=phonenumber)
            new_user.save()
            if new_user:
                result = sent_token(phone_number=phonenumber)

                return JsonResponse({
                    "status": "ok",
                    "description": "user created and" + result
                })
    else:
        return JsonResponse({
            "status": "bad paramater",
            "description": "your parameter is wrong"
        })


@csrf_exempt
def loginbycode(request):
    """
    logins user by code and phone number
    :param request:
    :return:
    """
    phone_number = request.POST.get('phone_number')
    code = request.POST.get('code')
    custom_auth = CustomAuth()
    if custom_auth.authenticatecode(phone_number=phone_number, code=code, request=request):
        user_phone_number = CustomUser.objects.get(phone_number=phone_number)
        new_token = generate_token()
        final_token = Token(token=new_token,
                            phone_number=user_phone_number,
                            token_start_date=datetime.datetime.now())
        final_token.save()
        return JsonResponse({
            "status": "ok",
            "is_authenticated": str(request.user.is_authenticated),
            "token": str(new_token)
        })
    else:
        return JsonResponse({
            "is_authenticated": str(request.user.is_authenticated),
            "description": "your credentials are wrong"
        })


def generate_token():
    """
    generates uuid token
    :return:
    """
    return str(uuid.uuid1())


def check_token_date_validation(token_start_date):
    """
    check token validation
    :param token_start_date:
    :return:
    """
    date_object = datetime.datetime.strptime(str(token_start_date), '%Y-%m-%d').date()
    delta = datetime.datetime.now().date() - date_object
    return delta.days


@csrf_exempt
def loginbytoken(request):
    """
    Authenticate User by token only
    :param request:
    :return: Json Response
    """
    new_token = request.POST.get('token')
    cusotm_auth = CustomAuth()
    description=cusotm_auth.authenticate_token(new_token, request)
    return JsonResponse({
        "description": description,
        "is_authenticated": str(request.user.is_authenticated)
    })
