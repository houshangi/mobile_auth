from ..models import Token, CustomUser


class SqlQuery():
    def get_phonenumber_from_token(self, final_token):
        phone_number_id = Token.objects.get(token=final_token).phone_number.id
        phone_number = CustomUser.objects.get(id=phone_number_id).phone_number
        return phone_number
