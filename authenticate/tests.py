import datetime
import json
from django.test import TestCase
from .models import Token, CustomUser



class AuthTestClass(TestCase):
    """token based and code based authentication unit testing"""

    def setUp(self):
        """Sets up variables for unit testing"""
        self.phone_number = "09365519522"
        self.code = 3345
        self.test_token = "testtokentest"
        self.wrong_code= "ttt21"
    def test_get_code_by_phone_number(self):
        """testing if code will be sent to pgone number"""
        response = self.client.post("/auth/sendcode",
                                    data={"phone_number": self.phone_number})
        resp = json.loads(str(response.content.decode("utf-8")))
        self.assertEqual(resp["status"], "ok")

    def test_code_login(self):
        """checking if token will be returend by giving code or not"""
        final_user = CustomUser(phone_number=self.phone_number)
        final_user.save()
        response = self.client.post(
            path="/auth/codelogin", data={"phone_number": self.phone_number,
                                          "code": self.code})
        resp = json.loads(str(response.content.decode("utf-8")))
        return self.assertEqual(resp["is_authenticated"], "True")

    def test_login_by_token(self):
        """testing login by token and token only"""
        final_user = CustomUser(phone_number=self.phone_number)
        final_user.save()
        user_phone_number = CustomUser.objects.get(phone_number=self.phone_number)
        new_token = self.test_token
        test_token = Token(token=new_token,
                           phone_number=user_phone_number,
                           token_start_date=datetime.datetime.now())
        test_token.save()
        response = self.client.post(
            path="/auth/tokenlogin", data={"token": self.test_token})
        resp = json.loads(str(response.content.decode("utf-8")))
        return self.assertEqual(resp["is_authenticated"], "True")

    def test_expired_token(self):
        """checks if token expiration works or not"""
        final_user = CustomUser(phone_number=self.phone_number)
        final_user.save()
        user_phone_number = CustomUser.objects.get(phone_number=self.phone_number)
        new_token = self.test_token
        test_token = Token(token=new_token,
                           phone_number=user_phone_number,
                           token_start_date=datetime.date(2019, 1, 1))
        test_token.save()
        response = self.client.post(
            path="/auth/tokenlogin", data={"token": self.test_token})
        resp = json.loads(str(response.content.decode("utf-8")))
        return self.assertEqual(resp["is_authenticated"], "False")

    def test_functionality_valid_scenario(self):
        """
        this function checks the whole process of sending phone number , generating token
        and logging in with token
        :return:
        """

        self.client.post("/auth/sendcode",
                                    data={"phone_number": self.phone_number})
        login_by_code_resp = self.client.post(
            path="/auth/codelogin",
            data={"phone_number": self.phone_number,"code":self.code})
        resp = json.loads(str(login_by_code_resp.content.decode("utf-8")))

        token_login_response = self.client.post(
            path="/auth/tokenlogin", data={"token": resp["token"]})
        resp=json.loads(str(token_login_response.content.decode("utf-8")))
        return self.assertEqual(resp["is_authenticated"], "True")

    def test_functionality_invalid_code(self):
        self.client.post("/auth/sendcode",
                         data={"phone_number": self.phone_number})

        login_by_code_resp = self.client.post(
            path="/auth/codelogin",
            data={"phone_number": self.phone_number, "code": self.wrong_code})
        resp = json.loads(str(login_by_code_resp.content.decode("utf-8")))
        return self.assertEqual(resp["is_authenticated"], "False")









