# Generated by Django 2.2 on 2019-10-29 20:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authenticate', '0004_auto_20191029_1635'),
    ]

    operations = [
        migrations.CreateModel(
            name='token',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.CharField(max_length=2000, unique=True)),
                ('token_start_date', models.DateField(verbose_name='token_start_date')),
            ],
        ),
        migrations.CreateModel(
            name='user',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone_number', models.CharField(max_length=200, unique=True)),
            ],
        ),
        migrations.DeleteModel(
            name='user_table',
        ),
        migrations.AddField(
            model_name='token',
            name='phone_number',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='authenticate.user'),
        ),
    ]
